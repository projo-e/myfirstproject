import React, { Component } from 'react'

export class Counter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 0
        }

    }

    render() {
        return (
            <div >
                <h1 className="text-center" >{this.state.count}</h1>
                <div className="text-center" >
                    <button className="btn btn-warnings" onClick={() => { this.setState({ count: this.state.count + 1 }) }}></button>
                    <button className="btn btn-primary" onClick={this.props.decrement}></button>

                </div>

            </div>
        )
    }
}

export default Counter