import logo from './logo.svg';
import './App.css';
import Counter from './apps/Counter';
import 'bootstrap/dist/css/bootstrap.css';

function App() {
  return (
    <div>
      <Counter />
    </div>
  );
}

export default App;
